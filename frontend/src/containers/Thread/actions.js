import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import { ADD_POST, LOAD_MORE_POSTS, SET_ALL_POSTS, SET_EXPANDED_POST } from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const deletePost = (postId, userId) => async (dispatch, getRootState) => {
  const response = postId ? await postService.deletePost(postId, userId) : undefined;
  if (!response) return;
  const mapDeletedPost = post => ({
    ...post,
    isDeleted: true
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDeletedPost(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDeletedPost(expandedPost)));
  }
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

const calculateReactionsOnLikeClick = previousPostReaction => {
  let likesDiff = 0;
  let dislikesDiff = 0;
  const isPreviousReactionExists = !!previousPostReaction?.id;
  if (isPreviousReactionExists) {
    if (previousPostReaction.isLike) {
      likesDiff -= 1;
    } else {
      likesDiff += 1;
      dislikesDiff -= 1;
    }
  } else {
    likesDiff += 1;
  }
  return [likesDiff, dislikesDiff];
};

const calculateReactionsOnDislikeClick = previousPostReaction => {
  let likesDiff = 0;
  let dislikesDiff = 0;
  const isPreviousReactionExists = !!previousPostReaction?.id;
  if (isPreviousReactionExists) {
    if (previousPostReaction.isLike) {
      dislikesDiff += 1;
      likesDiff -= 1;
    } else {
      dislikesDiff -= 1;
    }
  } else {
    dislikesDiff += 1;
  }
  return [likesDiff, dislikesDiff];
};

export const likePost = postId => async (dispatch, getRootState) => {
  const previousPostReaction = await postService.getPostReaction(postId);
  await postService.likePost(postId);
  const [likesDiff, dislikesDiff] = calculateReactionsOnLikeClick(previousPostReaction);
  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + likesDiff,
    dislikeCount: Number(post.dislikeCount) + dislikesDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const previousPostReaction = await postService.getPostReaction(postId);
  await postService.dislikePost(postId);
  const [likesDiff, dislikesDiff] = calculateReactionsOnDislikeClick(previousPostReaction);
  const mapDislikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + likesDiff,
    dislikeCount: Number(post.dislikeCount) + dislikesDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};
