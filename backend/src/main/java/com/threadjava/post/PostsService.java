package com.threadjava.post;

import com.threadjava.comment.CommentRepository;
import com.threadjava.post.dto.*;
import com.threadjava.post.model.Post;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostsService {

    private final PostsRepository postsCrudRepository;
    private final CommentRepository commentRepository;

    public PostsService(PostsRepository postsCrudRepository, CommentRepository commentRepository) {
        this.postsCrudRepository = postsCrudRepository;
        this.commentRepository = commentRepository;
    }

    public List<PostListDto> getAllPosts(Integer from, Integer count, UUID userId, Boolean hideOwnPosts,
                                         Boolean showOwnLikes) {
        final var pageable = PageRequest.of(from / count, count);
        if (showOwnLikes) {
            return postsCrudRepository
                    .findAllOwnLikedPosts(userId, pageable)
                    .stream()
                    .map(PostMapper.MAPPER::postListToPostListDto)
                    .collect(Collectors.toList());
        }
        if (hideOwnPosts == null || hideOwnPosts) {
            return postsCrudRepository
                    .findAllOtherUsersPosts(userId, pageable)
                    .stream()
                    .map(PostMapper.MAPPER::postListToPostListDto)
                    .collect(Collectors.toList());
        }
        return postsCrudRepository
                .findAllPosts(userId, pageable)
                .stream()
                .map(PostMapper.MAPPER::postListToPostListDto)
                .collect(Collectors.toList());
    }

    public PostDetailsDto getPostById(UUID id) {
        final var post = postsCrudRepository.findPostById(id)
                .map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow();

        final var comments = commentRepository.findAllByPostId(id)
                .stream()
                .map(PostMapper.MAPPER::commentToCommentDto)
                .collect(Collectors.toList());
        post.setComments(comments);

        return post;
    }

    public PostCreationResponseDto create(PostCreationDto postDto) {
        final var post = PostMapper.MAPPER.postDetailsDtoToPost(postDto);
        final var createdPost = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostCreationResponseDto(createdPost);
    }

    public void softDeletePostById(UUID id) {
        postsCrudRepository.softDeletePostById(id);
    }
}
