package com.threadjava.post;

import com.threadjava.post.dto.*;
import com.threadjava.post.model.Post;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {

    private final PostsService postsService;
    private final SimpMessagingTemplate template;

    public PostsController(PostsService postsService, SimpMessagingTemplate template) {
        this.postsService = postsService;
        this.template = template;
    }

    @GetMapping
    public List<PostListDto> get(@RequestParam(defaultValue = "0") Integer from,
                                 @RequestParam(defaultValue = "10") Integer count,
                                 @RequestParam(required = false) UUID userId,
                                 @RequestParam(required = false) Boolean hide,
                                 @RequestParam(required = false) Boolean like) {
        return postsService.getAllPosts(from, count, userId, hide, like);
    }

    @GetMapping("/{id}")
    public PostDetailsDto get(@PathVariable UUID id) {
        return postsService.getPostById(id);
    }

    @PostMapping
    public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.create(postDto);
        template.convertAndSend("/topic/new_post", item);
        return item;
    }

    @DeleteMapping("/{id}/{userId}")
    public HttpStatus delete(@PathVariable("id") UUID id, @PathVariable("userId") UUID userId) {
        if (!userId.equals(getUserId())) {
            return HttpStatus.UNAUTHORIZED;
        }
        postsService.softDeletePostById(id);
        return HttpStatus.OK;
    }
}
