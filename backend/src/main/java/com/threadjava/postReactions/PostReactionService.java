package com.threadjava.postReactions;

import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
public class PostReactionService {

    private final PostReactionsRepository postReactionsRepository;

    public PostReactionService(PostReactionsRepository postReactionsRepository) {
        this.postReactionsRepository = postReactionsRepository;
    }

    public Optional<ResponsePostReactionDto> getReaction(UUID userId, UUID postId) {
        return postReactionsRepository.getPostReaction(userId, postId)
                .map(PostReactionMapper.MAPPER::reactionToPostReactionDto);
    }

    public Optional<ResponsePostReactionDto> setReaction(ReceivedPostReactionDto newPostReaction) {
        final var previousReaction = postReactionsRepository.getPostReaction(
                newPostReaction.getUserId(), newPostReaction.getPostId()
        );
        if (previousReaction.isPresent()) {
            final var reaction = previousReaction.get();
            if (Objects.equals(reaction.getIsLike(), newPostReaction.getIsLike())) {
                postReactionsRepository.deleteById(reaction.getId());
                return Optional.empty();
            } else {
                reaction.setIsLike(newPostReaction.getIsLike());
                final var result = postReactionsRepository.save(reaction);
                return Optional.of(PostReactionMapper.MAPPER.reactionToPostReactionDto(result));
            }
        } else {
            final var postReaction = PostReactionMapper.MAPPER.dtoToPostReaction(newPostReaction);
            final var result = postReactionsRepository.save(postReaction);
            return Optional.of(PostReactionMapper.MAPPER.reactionToPostReactionDto(result));
        }
    }
}
