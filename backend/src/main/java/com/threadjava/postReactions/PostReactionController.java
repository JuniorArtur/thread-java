package com.threadjava.postReactions;

import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/postreaction")
public class PostReactionController {

    private final PostReactionService postsService;
    private final SimpMessagingTemplate template;

    @Autowired
    public PostReactionController(PostReactionService postsService, SimpMessagingTemplate template) {
        this.postsService = postsService;
        this.template = template;
    }

    @GetMapping("/{id}")
    public Optional<ResponsePostReactionDto> getReaction(@PathVariable("id") UUID postId){
        return postsService.getReaction(getUserId(), postId);
    }

    @PutMapping
    public Optional<ResponsePostReactionDto> setReaction(@RequestBody ReceivedPostReactionDto postReaction){
        postReaction.setUserId(getUserId());
        final var reaction = postsService.setReaction(postReaction);
        if (reaction.isPresent() && !reaction.get().getUserId().equals(getUserId())) {
            final boolean isLike = reaction.get().getIsLike();
            if (isLike) {
                template.convertAndSend("/topic/like", "Your post was liked!");
            } else {
                template.convertAndSend("/topic/dislike", "Your post was disliked!");
            }
        }
        return reaction;
    }
}
